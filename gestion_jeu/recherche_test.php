<!DOCTYPE html>
<html>
    <head>
   
        <title>Rechercher un jeu</title> 
	<?php	include '../include/header_public.php';  ?>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../CSS/CascadeStyleSheet.css">
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet">
    </head>
     

        <?php
	 
        include '../include/connexionbdd.php';

            $requete = "SELECT id_jeu, nom from jeux_video.jeu";
            $resultats = $connexion->query($requete);

	    // raz de la variable de session pour le jeu
	    $_SESSION['id_jeu']="";
            ?>
	<h1> Rechercher un jeu </h1>

  
   <form method='post' action='presentation.php'>
      <p>Jeux :   <select name='jeu' id='jeu'>
        <?php

  	   while($ligne = $resultats->fetch())
           {
	?>
		<option value="<?php echo $ligne['id_jeu']; ?>"><?php echo $ligne['nom']; ?></option>
	<?php }  ?>

</select>
 <button class="rechercher">Rechercher</button> </p>

</form>

<form method='post' >
     <p>Note :  <select class="note" type="text" name="note" value="note_min">
       <option value="1">1</option>
       <option>2</option>
       <option>3</option>
       <option>4</option>
       <option>5</option>
       <option>6</option>
       <option>7</option>
       <option>8</option>
       <option>9</option>
       <option>10</option>
     </select>  <button class="rechercher" name="btn_note">Rechercher</button> </p>
     </form>
          <?php
// lier la note choisit à la recherche  ==> quasi idem catégorie :  récupérer la note choisie dans la liste et trouver les jeux qui ont cette moyenne
// ==> dans la balise <table>
  ?>
  <form method="POST">
<?php

 $requete = "SELECT id, nom_categ from jeux_video.categorie order by nom_categ";
  $res_categ = $connexion->query($requete);
 ?>
       <p class="categoriech">Categories : <select class="categorie" name="categorie[]" multiple size="6">
<?php
       while($ligne = $res_categ->fetch())
           {
      ?>
      <option value="<?php echo $ligne['id']; ?>"><?php echo $ligne['nom_categ']; ?></option>
      <?php }  ?>

   </select>
 <button class="rechercher" name="btn_categ">Rechercher</button></form></p>

          	
<br><hr>

     <table  cellspacing="10">


             <?php    // faire en sorte que l'affichage des jeux en bas de page corresponde au(x) catégorie(s) choisie(s)
             if(isset($_POST["btn_categ"])  && !empty($_POST["categorie"])){
               $tab=$_POST["categorie"];

                $chaine="(";
                foreach($tab as $c){
                  $chaine.=$c.",";
                }
                $chaine.="0)";
                $requete1 ="select distinct(nom),image, description, id_jeu from jeux_video.jeu inner join jeux_video.categ_jeu on id_j = id_jeu where id_c in ".$chaine.";";
             }
	    else if (isset($_POST["btn_note"]) && !empty($_POST["note"])){
		     $requete1 = "select nom,image, description, id_jeu ,ROUND(avg(note))
				  from jeux_video.jeu
				  inner join jeux_video.donner_avis on donner_avis.jeu = jeu.id_jeu
				  group by nom,image, description, id_jeu
				  having ROUND(avg(note)) = ".$_POST['note'].";";
	    }
                 else {
            		$requete1 ="select image, description, nom,id_jeu from jeux_video.jeu";
                 }

              
            //lier au bouton rechercher */

             $resultat=$connexion->query($requete1);
             $i=0;
             while($i<9 && $ligne=$resultat->fetch() )
             {
                 $i++;
         
                ?>

       <tr>
         <td><img class="imgjeu" src="../images/jeux/<?php echo $ligne['image'];?>"     /></td>

         <td style="width: 100%;"><h3 class="titre">
             <?php       echo $ligne['nom'];        ?>

             </h3><h3 class="description"> Description :<br>  <?php
                    echo $ligne['description'];

              ?>
<br><br>
                        <?php
			// calcul de la note moyenne du jeu
                       $requete2 = "select id_jeu, ROUND ( avg (note)) as moy
                        from jeux_video.jeu
                        inner join jeux_video.donner_avis on donner_avis.jeu = jeu.id_jeu
                        where id_jeu=".$ligne['id_jeu'].
                        "group by id_jeu;";

                        $resultat2=$connexion->query($requete2);
                        $ligne2 = $resultat2->fetch();
			if ($ligne2["moy"]!="")
			{
			  echo " La note moyenne pour ce jeu est de : ".$ligne2['moy'];
			}
			else {
			  echo "pas de note pour ce jeu";
			}

 			// recherche des catégories du jeu
			$requete_categ = "select nom_categ from jeux_video.categorie
                        inner join jeux_video.categ_jeu on id = id_c
                        where id_j=".$ligne['id_jeu'].";";
                        
			echo "<br>Catégorie(s) du jeu : ";
                        $res_categorie=$connexion->query($requete_categ);
                        while($categs = $res_categorie->fetch())  {
                       		echo " - ".$categs['nom_categ'];
			}

			?> </h3></td>

       </tr>

   <?php
}
?>

   

  </table>
  <?php
	include '../include/footer_public.php';  ?>
    </body>
</html>
