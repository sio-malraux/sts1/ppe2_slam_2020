<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <?php include "../include/header_public.php";  ?>
        <title>Ajout jeu</title>
         <link rel="stylesheet" href="../CSS/ajout_jeu.css"/>
    </head>
    <body>
<?php include '../include/connexionbdd.php';
$requete = "SELECT id, nom FROM jeux_video.createur order by nom asc";
$resultats = $connexion->query($requete);
        ?>

        <h1>Ajouter un jeux</h1>
<!-- Retour après éventuel problème de maj pour affichage du message adéquate -->
		<?php
		        // si la variable $_GET[ " message " ] existe (envoyée par ajout_jeu_bdd.php  et n'est pas null
			if (isset($_GET["message"]))
			{
			   echo "<p style='color:red;text-align:center;'>".$_GET["message"]."</p>";
			} 
		?>
        <form method="POST" action="ajout_jeu_bdd.php" enctype="multipart/form-data">
        <p>
          <label for="photo"> <img class="image" src="../images/image_defaut.png" alt="téléch image"/></label> <br>
	  <input type="file" id="photo" name="photo"/>
        </p><br>
         
        
        <fieldset><legend>Informations</legend>
            <div class="info">
            <input type="text" name="nom_jeu" placeholder="Nom du jeu" required=""/>
	    <span style="margin-left:30px;">prix du jeu : </span><input style="margin-left:10px; width:2,5cm;" type="number" value=0 min=0 name="prix" required=""/> €
            <br><br>

            Créateur du jeux : <select name="createur[]" multiple size=3>
                <?php
	while($ligne=$resultats->fetch()) {
		echo "<option value='".$ligne['id']."'>".$ligne['nom']."</option>";
        }
	$resultats->closeCursor();
	?>

                             </select>
      
         <input style="margin-left:10px;" type="submit" name="nouveau" value="Ajouter un studio">
      
            <br> <br>  
            Date de sortie du jeux : <input type="date" name="date"/>
            <br><br> 
            Supports du jeux : <select name="plateforme[]" multiple size=3>

                 <?php
  $requete2 = "SELECT id_plateforme, libelle FROM jeux_video.plateforme order by libelle asc";
  $resultats2 = $connexion->query($requete2);
	while($ligne2=$resultats2->fetch()) {
		echo "<option value='".$ligne2['id_plateforme']."'>".$ligne2['libelle']."</option>";
        }
	$resultats2->closeCursor();
	unset($connexion);
	?>
            </select><br><br></div>
        </fieldset> 

        <fieldset><legend>Description</legend>
            <div class="desc">
            <textarea name="description" rows="10" cols="100" minlength="50" placeholder="Rentrer une description du jeu" required=""></textarea>
            </div>  
        </fieldset>
        
        <fieldset><legend>Configuration nécessaire</legend>
            <div class="config">
                Espace necessaire: <input type="text" name="hdd"  required=""/><br>
                Ram necessaire: <input type="text" name="ram" required=""/><br>
                Puissance necessaire: <input type="text" name="cpu" required=""/><br>
		Systèmes d'exploitation: <input type="text" name="se" required=""/><br>
                Materiel necessaire: <input type="text" name="matos" required=""/>

            </div>
        </fieldset><br>
        <input type="submit" value="Valider" style="margin:20px;"><input type="reset" name="reset" value="Reset">
     </form>
     <?php include "../include/footer_public.php";  ?>
  
    </body>
</html>
