<html>

      <head>
             <meta charset="utf-8">
    <title>Donner d'avis</title>
 <?php  include '../include/header_public.php';  ?>
      </head>
     
      <body>
 <?php
// page affichée seulement si on est connecté
if (isset($_SESSION['pseudo']) && $_SESSION['pseudo'] != null && $_SESSION['id_user']!=null) {
 include '../include/connexionbdd.php'; 
  if (isset($_POST['jeux'])) {$_SESSION['id_jeu']=$_POST['jeux']; } ?>

<br><br><h2 style="text-align:center;">Donnez votre avis</h2><br><br>
      <a href="presentation.php" style="margin-left:85%;">Précédent</a><br>
<form style="margin-left:20%;" action="inserer_avis.php" method="post">
Pseudo : <?php echo "<span style='font-style:italic; color:green;'>".$_SESSION['pseudo']."</span>";?> <br>
						
<p></p>Note générale :
<input type="number" name="note" size="2" value="" required minlength="1" maxlength="2" min="0" max="5"/> /5<br>

<p></p>Jeux : <select name="jeux">

<?php 
$requete = "SELECT nom, id_jeu FROM ppe2.jeux_video.jeu";
$resultats=$connexion->query($requete);
while ($ligne=$resultats -> fetch()) {
    if ($ligne['id_jeu']==$_POST['jeux']) { // si c'est le jeu de la page précédente, il faut le sélectionner par défaut
?>
	    <option selected value="<?php echo $ligne['id_jeu'];?>"> <?php echo $ligne['nom']; ?> </option>
<?php } 
	else { ?>
	    <option value="<?php echo $ligne['id_jeu'];?>"> <?php echo $ligne['nom']; ?> </option>
<?php } // fin ifelse
} // fin while ?>
</select><br>

 
<p></p>Votre avis <br>
<textarea id="avis" name="avis" row="5" cols="33" value="" required=""></textarea><br><br>

<p><button type="submit" class="button" name="envoyer">Envoyer</button></p><br>

<?php
	// si la variable $_GET[ " message " ] existe (envoyée par inserer_avis.php  et n'est pas null
	if (isset($_GET["message"]))
	{
		echo "<p style='color:red;'>".$_GET["message"]."</p>";
	}

// si la variable $_GET[ " erreur " ] existe (envoyée par inserer_avis.php  et n'est pas null
	if (isset($_GET["erreur"]))
	{
		echo "<p style='color:red;'>".$_GET["erreur"]."</p>";
	}
?>
<br><br></form>

<?php  include "../include/footer_public.php";  

}// fin if 
else
{
  echo '<h3 style="color:red; margin-left:10px;"> il faut être connecté pour accéder à cette page</h3>'; 
}?>

      </body>
</html>
