<!DOCTYPE html>
<html>
    <head>
        <title>Rechercher un jeu</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="CascadeStyleSheet.css">
        <link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet">
    </head>
    <body>

        <?php
        include './connexion.php';

        ?>
     <h1>Jeux : <input size="60" class="jeu" type="text" name="objreherche" placeholder="Rechercher un jeu"/> <button class="rechercher">Rechercher</button></h1>

     <h2>Note : <select class="note" type="text" name="note" value="note minimale souhaitée">
       <option value="1">1</option>
       <option>2</option>
       <option>3</option>
       <option>4</option>
       <option>5</option>
       <option>6</option>
       <option>7</option>
       <option>8</option>
       <option>9</option>
       <option>10</option>
     </select></h2>

      <h2 class="categoriech">Categories : <select class="categorie" name="type" multiple size="6">
       <option>Stratégie</option>
       <option>Solo</option>
       <option>Multijoueur</option>
       <option>Fun</option>
       <option>RPG</option>
       <option>Monde ouvert</option>
   </select></h2>
     
     <table>
             <?php
             $requete = "select image, description, nom  from jeux_video.jeu order by nom;";
             $resultat=$connexion->query($requete);
            $i=0;
             while($i<9 && $ligne=$resultat->fetch() )
             {
                 $i++;
        //   for($i=0; $i<=10; $i++)
         //  {
                ?>   
      
       <tr>    
         <td><img class="imgjeu" src=<?php       
               echo $ligne['image'];        
          ?>     /></td>
         <td style="width: 100%;"><h3 class="titre">
             <?php       echo $ligne['nom'];        ?>       
                       
             </h3><h3 class="description"> Description :<br><br>  <?php
                    echo $ligne['description'];
          ?>   
                       <br><br>85% des joueurs ont aimé ce jeu</h3></td>

       </tr>

   <?php
}      
?>
   
   <ul class="droit">

   </ul>

  </table>
    </body>
</html>
