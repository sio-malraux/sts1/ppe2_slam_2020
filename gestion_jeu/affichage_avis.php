<html>
<?php
    include "../include/header_public.php";
    ?>
<head>
    <meta charset="utf-8">
    <title>Page d'avis</title>
    <link rel="stylesheet" href="../CSS/affichage_avis.css">
  </head>
<body>
<div id="wrap">
<div id="main">

<h1><center>Page d'avis</center></h1>

<form action="affichage_avis.php" method="post">

<?php

include '../include/connexionbdd.php';

$requete = "SELECT id_jeu, nom FROM jeux_video.jeu";
$resultat = $connexion->query($requete);

?>

Jeux: <SELECT name = "jeu">
<?php 

while($ligne = $resultat->fetch())
{
?>

<?php if (isset($_POST['jeux']) && $_POST["jeu"] == $ligne['id_jeu']) {  ?>
  <option selected value="<?php echo $ligne['id_jeu']; ?>"> <?php echo $ligne['nom']; ?> </option>
<?php }// fin si else 
{ ?>
<option value="<?php echo $ligne['id_jeu']; ?>"> <?php echo $ligne['nom']; ?> </option>
<?php } // fin else 

 } ?>

<input type="submit" value="jeux" name="jeux">

</form>

<?php if(isset($_POST['jeux'])) {

$req = "SELECT nom FROM jeux_video.jeu WHERE id_jeu =".$_POST['jeu'].";";
$res = $connexion->query($req);

$jeu = $res -> fetch();

?>

Voici les avis pour le jeu <span style="font-weight:bold; font-style: italic;"><?php echo $jeu['nom']; ?> : </span>
<br></br>
<table>
<tr>
<th>Pseudo</th>
<th>Note</th>
<th>Date de l'avis</th>
<th>Commentaire</th>
</tr> 

<?php
$requete2 = "SELECT note, avis, pseudo, date, j.nom FROM jeux_video.donner_avis INNER JOIN jeux_video.utilisateur ON id_utilisateur = aviseur INNER JOIN jeux_video.jeu j ON id_jeu = jeu WHERE jeu = ".$_POST['jeu'].";";
$resultat2 = $connexion->query($requete2);

while($avis = $resultat2->fetch())
{
?>
<tr>
<td class='col_pseudo'><?php echo $avis['pseudo']; ?></td>
<td class='col_note'><?php echo $avis['note']; ?></td>
<td class='col_date'><?php echo $avis['date']; ?></td>
<td class='col_avis'><?php echo $avis['avis']; ?></td>
</tr> <?php
}
?>
</table>
<?php
}
?>
</div>
<div id="footer_avis">
<?php include "../include/footer_public.php";  ?>
</div>
</div> 
</body>

</html>
