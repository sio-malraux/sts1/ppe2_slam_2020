<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
<?php include "../include/header_public.php";  ?>
        	<title>Gestion des utilisateurs</title>
	        <link rel="stylesheet" href="../CSS/ajout_jeu.css"/>
		
	</head>
	
<body>
<?php if (isset($_SESSION['id_user'])) { ?>
  <br><h1 style="text-align:center;"> Modification du profil </h1><br>
<section style="margin-left:10px;">
<!-- Retour après éventuel problème de maj pour affichage du message adéquate -->
		<?php
		        // si la variable $_GET[ " message " ] existe (envoyée par usermodification.php  et n'est pas null
			if (isset($_GET["message"]))
			{
			   echo "<p style='color:red;text-align:center;'>".$_GET["message"]."</p>";
			   $_POST['id_modif'] = $_SESSION['id_modif'];
			} 
		?>
<form method="post" action="usermodification_bdd.php"  enctype="multipart/form-data" >  <!-- enctype ==> pour le fichier image  -->
<fieldset>
	<div class="premier">
		<div class="image_pro">
    			<?php
			include '../include/connexionbdd.php';
    		        $requete="select * from jeux_video.utilisateur where id_utilisateur = ".$_SESSION['id_user'];
                        $resultats= $connexion->query($requete);
			$ligne=$resultats->fetch();
			?>
    			<p><img class="img_profil" style="margin-left:10px; width: 200px; height: 200px;"src="../images/profils/<?php echo $ligne['image_profil'];?>"/></p>    			
                        <p><label style="margin-left:10px;">Modifier l'image</label> : <input type="file" name="fichier_image" /><p>
		</div>
       </div>
</fieldset>

<fieldset >
      <legend style="font-weight:bold;">Vos données publiques</legend>  
    		<div style="margin-left:10px;" class="info_public">
    			<div class="pseudo"><p><label>  Pseudo</label> : <input type="text" name="p_pseudo" value="<?php echo $ligne['pseudo']; ?>" /></p>
    			</div>
    			<?php // recherche des nationalités dans la bdd
					 $requete_nat="SELECT * FROM jeux_video.nationalite;";
					 $res_nat = $connexion->query($requete_nat);
			?>
			<div class="nation"><p><label>  Nationalité</label> :
							 <select name='nation'>
							<?php   // affichage des nationalités possibles"
								while($ligne_nat = $res_nat->fetch()) {
							 	    if ($ligne_nat['id_nationalite']==$ligne['nationalite']) {  // si c'est la nationalité de l'utilisateur à modifier, on sélectionne ?>
								 <option selected value="<?php echo $ligne_nat['id_nationalite']; ?>"><?php echo $ligne_nat['libelle']; ?></option>
							<?php 	    } // fin si 
								   else { ?>
								<option value="<?php echo $ligne_nat['id_nationalite']; ?>"><?php echo $ligne_nat['libelle']; ?></option>
							<?php 	    } // fin else
								} // fin while
							
					// $res_nat->closeCursor();
					 ?></select></p>
    			</div>
    			
                        <div class="site_name"><p><label>  Site Perso</label> : <input type="text" name="site" value="<?php echo $ligne['site_web_perso']; ?>" /></p>
    			</div>
    		</div>
</fieldset>

<fieldset >
     <legend style="font-weight:bold;">Les données privées</legend>
      <div style="margin-left:10px;" class="info_private">
	<div class="private_1"><p><label>  Nom</label> : <input type="text" name="nom" value="<?php  echo $ligne['nom'];?>"/>
	 <label style="margin-left:20px;">  Prénom</label> : <input type="text" name="prenom" value="<?php echo $ligne['prenom'];?>"/></p>
	</div>

	<div class="private_5"><p><label>  Mot de passe</label> : <input type="password" name="mdp" value="<?php echo $ligne['mdp'];?>"/></p>
        </div>
	
	<div class="private_3"><p><label>  Modifier l email</label> : <input type="text" name="email" value="<?php echo $ligne['email']; ?>"/></p>
	</div>
      
       
	<div class="private_4"><br><p><label>  Date de naissance</label> : <input type="date" name="date_naissance" value="<?php echo $ligne['date_naiss']; ?>"/></p>
	</div></div>
</fieldset>

<fieldset>
	<legend style="font-weight:bold;" >Biographie</legend>
		<div style="margin-left:10px;" class="bio"><textarea type="text" name="biographie"><?php echo $ligne['biblio'];?></textarea><br>
    		</div>	
</fieldset>

<fieldset>
	<legend style="font-weight:bold;">Confirmation</legend>
		<div style="margin-left:10px; text-align:center;" class="confirmation">
			<input type='hidden' name="id_modif" value='<?php echo $ligne['id_utilisateur']; ?>' /> 
			<input type="submit" value="Valider" name="save" /><br>
		</div><br>
</fieldset>
			
</form>
</section><br>
<?php
	include '../include/footer_public.php';
} // fin si var $_SESSION existe
 ?>
</body>
</html>

