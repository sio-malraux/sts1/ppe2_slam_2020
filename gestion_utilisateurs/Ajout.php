<!DOCTYPE html>
<html> 
	<head>
		<title>Fiche inscription</title>
		<meta charset="UTF-8">
		<!--<meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
	<?php
	include '../include/header_public.php';
    	?>
	</head>
	<body>
		<h1>Ajout d'utilisateur : </h1>
		<form action="requeteAjout.php" method="post" enctype="multipart/form-data">

			<p>
				<input type="file" name="avatar" accept="image/png, image/jpeg, image/gif"/>
			</p>
			<p>
				<label for="name">Nom : </label>
				<input type="text" name="nom"/>
			</p>

			<p>
				<label for="name">Prénom : </label>
				<input type="text" name="prenom"/>
			</p>

			<p>
				<label for="name">Pseudo : </label>
				<input type="text" name="pseudo"/>
			</p>

			<p>
				<label for="name">Mot de passe : </label>
				<input type="password" name="mdp"/>
			</p>

			<p>
				<?php
				$requete_nat="SELECT * FROM jeux_video.nationalite;";
				$res_nat = $connexion->query($requete_nat);
				?>
				<p>
				<label>  Nationalité</label> :
				<select name='nation'>
				<?php   // affichage des nationalités possibles"
				while($ligne_nat = $res_nat->fetch())
				{
				?>
					<option value="<?php echo $ligne_nat['id_nationalite']; ?>">
					<?php
					echo $ligne_nat['libelle'];
					?>
					</option>
					<?php
				} // fin while    }
				?>
				</select>
			</p>
			</p>

			<p>
				<label for="name">Date de naissance : </label>
				<input type="Date" name="dateNaiss"/>
			</p>

			<p>
				<label for="name">Twitter : </label>
				<input type="texte" name="twitter"/>
			</p>

			<p>
				<label for="name">Site Web : </label>
				<input type="texte" name="siteWeb"/>
			</p>


			<p>
				<label for="name">Bibliographie :  </label>
				<textarea name="bio"></textarea>
			</p>

			<p>
				<label for="name">Adresse mail : </label>
				<input type="text" name="mail"/>
			</p>

			<p>
				<label for="name">Rôle : </label>
				<input type="text" name="role"/>
			</p>

			<p>
                <input type="submit" value="Confirmer" />
            </p>
 
		</form>
		<?php include "../include/footer_public.php";  ?>
   </body>
</html>
