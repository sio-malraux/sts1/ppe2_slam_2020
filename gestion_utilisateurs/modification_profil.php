<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
<?php include "../include/header_public.php";  ?>
        	<title>Gestion des utilisateurs</title>
		<link href="../CSS/Css-Affichage.css" rel="stylesheet">
	</head>

<body>
  <h1> Modification du profil </h1>
<section>
<!-- Retour après éventuel problème de maj pour affichage du message adéquate -->
		<?php
		        // si la variable $_GET[ " message " ] existe (envoyée par modif_profil_bdd.php  et n'est pas null
			if (isset($_GET["message"]))
			{
			   echo "<p style='color:red;text-align:center;'>".$_GET["message"]."</p>";
			   $_POST['id_modif'] = $_SESSION['id_modif'];
			} 
		?>
<form method="post" action="modif_profil_bdd.php"  enctype="multipart/form-data" >  <!-- enctype ==> pour le fichier image  -->
<fieldset>
	<div class="premier">
		<div class="image_pro">
    			<?php
			include '../include/connexionbdd.php';
    		        $requete="select * from jeux_video.utilisateur where id_utilisateur = ".$_POST['id_modif'];
                        $resultats= $connexion->query($requete);
			$ligne=$resultats->fetch();
			?>
    			<p><img class="img_profil" src="../images/profils/<?php echo $ligne['image_profil'];?>"/></p>
    			
                        <p><label>Modifier l'image</label> : <input type="file" name="fichier_image" /><p>
		</div>
       </div>
</fieldset>

<fieldset >
      <legend>Vos données publiques</legend>  
    		<div class="info_public">
    			<div class="pseudo"><p><label>  Pseudo</label> : <input type="text" name="p_pseudo" value="<?php echo $ligne['pseudo']; ?>" /></p>
    			</div>
    			<?php // recherche des nationalités dans la bdd
					 $requete_nat="SELECT * FROM jeux_video.nationalite;";
					 $res_nat = $connexion->query($requete_nat);
			?>
			<div class="nation"><p><label>  Nationalité</label> :
							 <select name='nation'>
							<?php   // affichage des nationalités possibles"
								while($ligne_nat = $res_nat->fetch()) {
							 	    if ($ligne_nat['id_nationalite']==$ligne['nationalite']) {  // si c'est la nationalité de l'utilisateur à modifier, on sélectionne ?>
								 <option selected value="<?php echo $ligne_nat['id_nationalite']; ?>"><?php echo $ligne_nat['libelle']; ?></option>
							<?php 	    } // fin si 
								   else { ?>
								<option value="<?php echo $ligne_nat['id_nationalite']; ?>"><?php echo $ligne_nat['libelle']; ?></option>
							<?php 	    } // fin else
								} // fin while
							
					// $res_nat->closeCursor();
					 ?></select></p>
    			</div>
    			
                        <div class="site_name"><p><label>  Site Perso</label> : <input type="text" name="site" value="<?php echo $ligne['site_web_perso']; ?>" /></p>
    			</div>
    		</div>
</fieldset>

<fieldset >
     <legend>Les données privées</legend>
      <div class="info_private">
	<div class="private_1"><p><label>  Nom</label> :<input type="text" name="nom" value="<?php  echo $ligne['nom'];?>"/></p>
	</div>

	<div class="private_2"><p><label>  Prénom</label> : <input type="text" name="prenom" value="<?php echo $ligne['prenom'];?>"/></p>
	</div>

	<div class="private_3"><p><label>  Modifier l'email</label> : <input type="text" name="email" value="<?php echo $ligne['email']; ?>"/></p>
	</div>
      </div>
       
	<div class="private_4"><br><p><label>  Date de naissance</label> : <input type="date" name="date_naissance" value="<?php echo $ligne['date_naiss']; ?>"/></p>
	</div>
</fieldset>

<fieldset>
	<legend>Biographie</legend>
		<div class="bio"><textarea type="text" name="biographie"><?php echo $ligne['biblio'];
										$resultats->closeCursor();
										unset($connexion);	
								 	 ?>
				</textarea>
    		</div>	
</fieldset>

<fieldset>
	<legend>Confirmation</legend>
		<div class="confirmation">
			<input type="submit" value="Valider" name="wesh" />
			<input type='hidden' name="id_modif" value='<?php echo $_POST['id_modif']; ?>' /> 
		</div>
</fieldset>
			
</form>
</section>
<?php
	include '../include/footer_public.php';  ?>
</body>
</html>

