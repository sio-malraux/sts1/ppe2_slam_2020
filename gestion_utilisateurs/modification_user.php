<!DOCTYPE html> <html> <head> <meta charset="utf-8" /> <title>Modifier (ancien)</title> <link href="test.css" rel="stylesheet"> </head> <body> <section> <form method="post" action="fichier.php">
    		
    			<fieldset> <div class="premier"> <div class="image_pro">
						
							<p> <img class="img_profil" src=" <?php include '../include/connexionbdd.php'; $requete="select image_profil from jeux_video.utilisateur where id_utilisateur = 
									1";
									
									$resultats= $connexion->query($requete);
									
									if($ligne=$resultats->fetch()) { echo $ligne['image_profil'];
									}
									
									$resultats->closeCursor();
									
									unset($connexion); ?> "/> </p> <p> <label>Modifier l'image : </label> <input type="file" name="image"/>
								
								<!-- Relié à la base de donnée --> </p> </div> <div class="bio"> <p>Biographie actuelle : <?php include './connexion.php'; $requete="select 
							biblio from jeux_video.utilisateur where id_utilisateur=1";
							
							$resultats= $connexion->query($requete);
							
							if($ligne=$resultats->fetch()) { echo $ligne['biblio'];
							}
							
							$resultats->closeCursor();
							
							unset($connexion); ?> </p> <!-- Relié à la base de donnée --> <p> <label>Modifier la biographie : </label> <textarea type="text" name="biographie" 
								/></textarea>
							</p> </div> </div> </fieldset>
    			
    			
    			<fieldset class="ldp"> <legend>Les données publiques</legend> <div class="info_public"> <div class="pseudo"> <p> Pseudo actuel : <?php include './connexion.php'; $requete="SELECT 
							pseudo FROM jeux_video.utilisateur WHERE id_utilisateur=1";
							
							$resultats= $connexion->query($requete);
							
							if($ligne=$resultats->fetch()) { echo $ligne['pseudo'];
							}
							
							$resultats->closeCursor();
							
							unset($connexion); ?> </p> <!-- Relié à la base de donnée --> <p> <label>Modifier le pseudo : </label> <input type="text" name="pseudo" /> </p> 
						</div> <div class="nation">
							<p> Nationalité actuelle :
							
							<?php include './connexion.php' ?> <?php $requete="SELECT nationalite.libelle FROM jeux_video.utilisateur INNER JOIN jeux_video.nationalite ON 
							utilisateur.nationalite = nationalite.id_nationalite WHERE id_utilisateur = 1";
							
							$resultat = $connexion->query($requete);
							
							$ligne = $resultat->fetch();
							
							echo $ligne['libelle'];
							
							$resultats->closeCursor();
							
							unset($connexion); ?>
							
							</p> <!-- Relié à la base de donnée --> <p><label>Modifier la nationalité</label> : <input type="text" name="nation" /><p> </div> <div 
						class="site_name">
							<p> Nom site actuel :
							
							<?php include './connexion.php' ?> <?php $requete="SELECT site_web_perso FROM jeux_video.utilisateur WHERE id_utilisateur = 1";
							
							$resultat = $connexion->query($requete);
							
							$ligne = $resultat->fetch();
							
							if(isset($ligne['site_web_perso'])) { echo $ligne['site_web_perso'];
							}
							
							$resultats->closeCursor();
							
							unset($connexion); ?>
							
							</p> <!-- Relié à la base de donnée --> <p> <label>Modifier le lien : </label> <input type="text" name="site"/> <p> </div> </div> </fieldset>
    			
    			
    			<fieldset class="ldpr"> <legend>Les données privées</legend> <div class="private_1"> <div class="nom"> <p>Nom actuel :
							
							<?php include './connexion.php'; $requete="select nom from jeux_video.utilisateur where id_utilisateur=1";
							
							$resultats= $connexion->query($requete);
							
							if($ligne=$resultats->fetch()) { echo $ligne['nom'];
							}
							
							$resultats->closeCursor();
							
							unset($connexion); ?> </p> <!-- Relié à la base de donnée --> <p> <label>Modifier le nom : </label> <input type="text" name="nom"/> <p> </div> <div 
						class="prenom">
							<p>Prénom actuel :
							
							<?php include './connexion.php' ?> <?php $requete="SELECT prenom FROM jeux_video.utilisateur WHERE id_utilisateur = 1";
							
							$resultat = $connexion->query($requete);
							
							$ligne = $resultat->fetch();
							
							echo $ligne['prenom'];
							
							$resultats->closeCursor();
							
							unset($connexion); ?>
							
							</p> <!-- Relié à la base de donnée --> <p> <label>Modifier le prénom : </label> <input type="text" name="prenom"/> </p> </div> <div class="pwd"> 
							<p>Mot de passe : confidentiel</p> <p>
								<label>Modifier le mot de passe : </label> <input type="password" name="mdp"/> </p> </div> </div>
					
					
					<div class="private_2"> <div class="email"> <p>Email actuelle :
							
							<?php include './connexion.php' ?> <?php $requete="SELECT email FROM jeux_video.utilisateur WHERE id_utilisateur = 1";
							
							$resultat = $connexion->query($requete);
							
							$ligne = $resultat->fetch();
							
							echo $ligne['email'];
							
							$resultats->closeCursor();
							
							unset($connexion); ?>
							
							</p> <!-- Relié à la base de donnée --> <p> <label>Modifier l'email : </label> <input type="text" name="email"/> </p> </div>
						
						<div class="date_naiss"> <p>Date de naissance actuelle :
							
							<?php include './connexion.php' ?> <?php $requete="SELECT date_naiss FROM jeux_video.utilisateur WHERE id_utilisateur = 1";
							
							$resultat = $connexion->query($requete);
							
							$ligne = $resultat->fetch();
							
							echo $ligne['date_naiss'];
							
							$resultats->closeCursor();
							
							unset($connexion); ?>
							
							</p> <!-- Relié à la base de donnée --> <p> <label>Modifier la date de naissance : </label> <input type="date" name="date_naissance" /> </p> </div> 
						<!-- <div class="date_date_inscrip">
							<p> Date d'inscription actuelle :
							
							<?php include './connexion.php' ?> <?php $requete="SELECT date_debut_membre FROM jeux_video.utilisateur WHERE id_utilisateur = 1";
							
							$resultat = $connexion->query($requete);
							
							$ligne = $resultat->fetch();
							
							echo $ligne['date_debut_membre']; ?>
							
							</p> <p> <label>Modifier la date d'inscription : </label> <input type="date" name="date_inscription" /> </p> </div>--> </div> </fieldset>
    			
    			
				<fieldset> <legend>Confirmation</legend> <div class="confirmation"> <div class="soumettre"> <input type="submit" value="Enregistrer" /> </div> <div class="reset"> <input 
							type="reset" value="Reset" />
						</div> <div class="supprimer"> <input type="button" value="Supprimer" /> </div> </div> </fieldset>
    			
    		</form> </section> </body> </html>
