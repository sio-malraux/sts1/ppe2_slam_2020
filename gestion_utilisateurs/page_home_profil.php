<html>
    <head>
        <title>Les Profils</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php 	   include '../include/header_public.php';  ?>	
        <style>
         .button {
         background-color: #23b5b5;
         border: none;
         color: #white;
         padding: 15px 20px;
         text-align: center;
         text-decoration: none;
         display: inline-block;
         font-size: 15px;
         cursor: pointer;
         float:right;
         }

 	.nav { display:block; }
      </style>


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
 <!--   <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>  -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

 <style>
         .button {
         background-color: #23b5b5;
         border: none;
         color: white;
         padding: 15px 20px;
         text-align: center;
         text-decoration: none;
         display: inline-block;
         font-size: 15px;
         cursor: pointer;
         float:right;
         }
      </style>


	</head>

    <body style ="background-color:#f5f5f7">

            <h1 style="margin-top:25px; color : #23b5b5; text-align:center;">
            Page d'accueil profils<br><br> </h1>

            <?php

            include '../include/connexionbdd.php';

		$requete = "select * from jeux_video.utilisateur";
		$resultats = $connexion->query($requete);
?>

	<div class="container">

	<div class="table-responsive">

	 <table id="tab" bgcolor="#dedcdc" style= text-align:left; width="95%" border="2" cellpadding="0" cellspacing="60">

	<thead>

  	<tr>
		<th>Image profil</th>
		<th>Pseudo</th>
	 	<th>Nationalité</th>
		<th>Jeux préféré(s)</th>
	</tr>
	</thead>

	<tbody>

	<?php
		 while($ligne = $resultats->fetch())
             {
            ?>


                <tr  tabindex="0" aria-controls="example" role="row">

		<td align="center">
                <form style="margin:0px;" action="page_profil.php" method="post">

		     <button type="submit" id="user" name="user" value="<?php echo $ligne['id_utilisateur']; ?>" >
			  <img style="width: 200px; height:200px;" src="../images/profils/<?php echo $ligne['image_profil']; ?>" /> 
		     </button>

                </form>
                </td>

                <td>
                    <u><strong>pseudo :</strong></u>  <?php echo $ligne['pseudo']; ?>

	        </td>
                        <?php
			if($ligne['nationalite'] != null){
			 $req="select libelle from jeux_video.nationalite where id_nationalite=".$ligne['nationalite'];
                              $res=$connexion->query($req);
                              $nat=$res->fetch();

                        ?>

		<td>
                    <u><strong>nationalité :</strong></u>  <?php echo $nat['libelle'];  ?>
		</td>

	<?php } else { ?>

			<td>
		<strong>PAS DE NATIONALITE</strong>
			</td>

	<?php } ?>
                    <td>
		<?php
                        $req="select jeu.nom from  jeux_video.avoir_pour_favoris inner join jeux_video.jeu on jeu.id_jeu = avoir_pour_favoris.jeu where avoir_pour_favoris.joueur= ".$ligne['id_utilisateur'];
                        $res2=$connexion->query($req);
			$nom_jp = $res2->fetch();


			if ($nom_jp['nom'] == "")
            		{?>
                		<strong> PAS DE JEUX </strong> <?php
            		}
            		else
            		{
                	?>
                		<u><strong>jeux préféré(s) :</strong></u> <?php
                		do {
                		echo $nom_jp['nom']." - ";
                		} while($nom_jp = $res2->fetch());
            		}



                    ?>
		</td>
            </tr>
	<?php } ?>

           </tbody>
        </table>
        <br>

        <br><br><br>


	<script>
$(document).ready(function() {
    $('#tab').DataTable( {
        "language": {
         "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        },
        "order": [[ 3, "desc" ]]
    } );
} );

	</script>

        <a href="../index.php" class="button">Retour à l'accueil</a>

        <br><br><br><br>

	</div>
	</div>

    

		<?php  include "../include/footer_public.php";  ?>
</body>
</html>
