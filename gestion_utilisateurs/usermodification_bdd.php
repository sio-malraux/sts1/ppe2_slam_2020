<?php
session_start();
include '../include/connexionbdd.php';

$_SESSION['id_modif']=$_POST['id_modif'];  // il faut revenir à la page de modif avec l'id de l'utilisateur à modifier

// le "nouveau pseudo" éventuellement modifié existe-t-il déjà dans la bdd ?
$req_test_pseudo = "select pseudo from jeux_video.utilisateur where pseudo = '".$_POST['p_pseudo']."' 
								 and id_utilisateur <> ".$_POST['id_modif'].";";
$res_test_pseudo = $connexion->query($req_test_pseudo)->fetch();
if ($res_test_pseudo['pseudo']!=NULL) {
      
      header("Location: usermodification.php?message=pseudo déjà existant !!");
}
else
{
	// le "nouvel email" éventuellement modifié existe-t-il déjà dans la bdd ?
	$req_test_email = "select email from jeux_video.utilisateur where email = '".$_POST['email']."' 
								 and id_utilisateur <> ".$_POST['id_modif'].";";
	$res_test_email = $connexion->query($req_test_email)->fetch();
	if ($res_test_email['email']!=NULL) {
	      $_SESSION['id_modif']=$_POST['id_modif']; // il faut revenir à la page de modif avec l'id de l'utilisateur à modifier
	      header("Location: usermodification.php?message=email déjà existant !!");
	}
	else {
		// est-ce qu'il y a une image de profil à changer ?
		if (isset($_FILES['fichier_image']) && $_FILES['fichier_image']['error']==0) {
			// Testons si le fichier n'est pas trop gros (moins de 1Mo ici)
       			if ($_FILES['fichier_image']['size'] <= 1000000)
       			{
				// Testons si l'extension est autorisée
			       	$infosfichier = pathinfo($_FILES['fichier_image']['name']);
				$extension_upload = $infosfichier['extension'];
		       	        $extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png');
		       	        if (in_array($extension_upload, $extensions_autorisees))
		       	        {
					// On peut valider le fichier et le stocker définitivement
					move_uploaded_file($_FILES['fichier_image']['tmp_name'], '../images/profils/'.basename($_POST['p_pseudo'].".".$infosfichier['extension']));
					// update pour stockage sur postgresql 
					$req_update1 = "update jeux_video.utilisateur set image_profil = '".$_POST['p_pseudo'].".".$infosfichier['extension']."' where pseudo = '".	$_POST['p_pseudo']."';";
					$update1 = $connexion->exec($req_update1);

					// puis update des autres données éventuellement modifiées :
					$req_update2 = "update jeux_video.utilisateur set  nom='".$_POST['nom']."', 
							  prenom='".$_POST['prenom']."', 
							  email='".$_POST['email']."', 
							  pseudo='".$_POST['p_pseudo']."', 
							  date_naiss='".$_POST['date_naissance']."', 
							  nationalite='".$_POST['nation']."',
							  site_web_perso='".$_POST['site']."',
							  mdp='".$_POST['mdp']."', 
							  biblio='".$_POST['biographie']."'          where id_utilisateur = ".$_POST['id_modif'].";";
					$update2 = $connexion->exec($req_update2);

					$_SESSION['pseudo']=$_POST['p_pseudo'];
					// retour à la page admin user
					header("Location: usermodification.php?message=le profil de pseudo ".$_POST['p_pseudo']." a bien été modifié !! ");					
		 	        }
				else {  // extension non ok
					header("Location: usermodification.php?message=type d'image non accepté (choisir jpg, jpeg,gif ou png)!");
				} 			
        		}
			else { // fichier image trop lourd
				header("Location: usermodification.php?message=fichier image trop volumineux !!");
			}
	    } // fin si fichier
	    else // juste maj données 
	    {
		$req_update2 = "update jeux_video.utilisateur set  nom='".$_POST['nom']."', 
								  prenom='".$_POST['prenom']."', 
								  email='".$_POST['email']."', 
								  pseudo='".$_POST['p_pseudo']."', 
								  date_naiss='".$_POST['date_naissance']."', 
								  nationalite='".$_POST['nation']."',
								  site_web_perso='".$_POST['site']."',
							 	  mdp='".$_POST['mdp']."',  
								  biblio='rrrrrrr' where id_utilisateur = ".$_POST['id_modif'].";";
		$update2 = $connexion->exec($req_update2);

		// retour à la page admin user
                $_SESSION['pseudo']=$_POST['p_pseudo'];
		header("Location: usermodification.php?message=le profil de pseudo ".$_POST['p_pseudo']." a bien été modifié !! ");
	   } // fin else fichier
  } //fin else email
} // fin else pseudo

?>
