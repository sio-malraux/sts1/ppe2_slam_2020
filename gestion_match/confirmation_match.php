<?php
	include'../include/connexionbdd.php';
	
	// on gère la confirmation ou l'annulation :

	if (isset($_POST['annul'])) { 
		// retour à la page des matchs
		header("Location: competition_show.php? ");		
	
	}
	if (isset($_POST['confirm'])) {
		// enregistrement de l'inscription et retour à la page accueil 

		// 1- récup de l'id du joueur connecté (grace à son pseudo)
		$requete="SELECT id_utilisateur FROM jeux_video.utilisateur where pseudo='".$_POST['pseudo']."';";
		echo $requete;
	   	$resultats= $connexion->query($requete);
   		$ligne=$resultats->fetch();
		$resultats->closeCursor();

		// 2- on peut alors insérer 
		$req_insert="INSERT INTO jeux_video.inscrire (match, joueur) VALUES (".$_POST['match'].",".$ligne['id_utilisateur'].");";
	   	$insert = $connexion->exec($req_insert);
   		 
		// retour à la page des matchs
		header("Location: competition_show.php?message=inscription enregistrée ! ");	
	}
?>
<html style ="height:100%;" >
    <head>
        <meta charset="UTF-8">
	<?php include "../include/header_public.php";  ?>
	<title>Confirmation</title>
	<link rel="stylesheet" href="../CSS/affichage_compet.css">
    </head>
    

<body>
<?php
	// on affiche cette page que si on est connecté !!!
	if (isset($_SESSION['pseudo'])) {
?>
<div id="wrap">
   <div id="main">
<br><br>
    <center><h2> Confirmation - Rejoindre un match </h2></center><br><br>

    <?php			
	if (!(isset($_POST['confirm'])) && !(isset($_POST['annul']))) {
		
		$requete="SELECT date_debut, libelle, nom FROM jeux_video.match m inner join jeux_video.competition c on m.compet = c.id 
									     inner join jeux_video.jeu j on j.id_jeu = c.id_jeu
									     where m.id=".$_POST['match_id'].";";
	   	$resultats= $connexion->query($requete);
   		$ligne=$resultats->fetch();
		$resultats->closeCursor();
    ?>  
    	
    
       <center>
 	<p style="color:red;padding:5px;">   <?php echo " >>> ".$_SESSION['pseudo']. " <<< "; ?> <br><br>Confirmez-vous que vous souhaitez vous inscrire pour le match <?php echo $_POST['match_id']; ?> : <br> compétition "<?php echo $ligne['libelle']; ?>", <br>concernant le jeu <?php echo $ligne['nom']; ?> <br> et débutant le : <?php echo $ligne['date_debut']; ?></p>
   	 

    <form method="post" action="confirmation_match.php">   	 
    	<br><p><input style="margin-right:20px;" id="confirm" name="confirm" type="submit" value="je confirme"/>
	<input type="hidden" name="match" id="match" value="<?php echo $_POST['match_id'];?>"/>
	<input type="hidden" name="pseudo" id="pseudo" value="<?php echo $_SESSION['pseudo'];?>"/>
	<input id="annul" name="annul" type="submit" value="annuler"/></p>
     
    </form> </center>
	<?php } } // fin du if vérif si on est connecté 

	else 
	{
		echo "IL FAUT ETRE CONNECTE POUR ACCEDER A CETTE PAGE";
	} ?>


	
	</body>
</html>


