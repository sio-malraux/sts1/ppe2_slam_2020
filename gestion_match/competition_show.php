<!DOCTYPE html>
<html style ="height:100%;" >
    <head>
        <meta charset="UTF-8">
        
<?php include "../include/header_public.php";  ?>
    <title>Compétitions</title>
	<!--  <link rel="stylesheet" href="../CSS/competition_show.css">-->
<link rel="stylesheet" href="../CSS/affichage_compet.css">
         </head>
    

<body  class="competition" >
<div id="wrap">
	<div id="main">

    <center><h2> Compétitions </h2></center>
        
        
            <?php include'../include/connexionbdd.php';
            
            ?>
	<br><h3>Liste des matchs : </h3>
            <table style="margin: 0px auto 10px auto;"border cellpadding=5>
            <tr style="text-align:center;"> 
                <th>  nom de la compétition 	</th>
                <th>  nom du jeu       		</th>
                <th>  date fin inscription      </th>
                <th>  date match                </th>
                <th>  nombre joueurs minimum    </th>
                <th>  nombre joueurs maximum	</th>
                <th>  nombre joueurs inscrits   </th>
                <th>  types de la compétition   </th>
            </tr>
            
               <?php // ici recherche de toutes les compétitions ( ou celles d'un jeu - à voir ?) en bdd dans la table des matchs :

		$requete="SELECT c.libelle as lib_c, j.nom as nom_j, m.id as match_id, date_debut, (date_debut-7) as date_ins, nb_joueur_min, nb_joueur_max, tc.libelle as lib_type
			  FROM jeux_video.match m
			  inner join jeux_video.competition c on m.compet=c.id
			  inner join jeux_video.jeu j on j.id_jeu = c.id_jeu
			  inner join jeux_video.compet_etre_type cet on c.id = cet.compet
			  inner join jeux_video.type_compet tc on tc.id = cet.type_c
			  order by c.libelle;";

                $resultats=$connexion->query($requete);

		// construction d'une ligne du tableau avec les infos récupérées :
                    while($nom=$resultats->fetch()) {  
			// recherche du nombre d'inscrits pour le match en lecture ici
			$rech_inscrit = "SELECT count(*) as nb FROM jeux_video.inscrire WHERE match = ".$nom['match_id'].";";
			$res_nb_inscrit= $connexion->query($rech_inscrit)->fetch(); ?>
			<tr>
				<td><?php echo $nom['lib_c']; ?></td>
				<td><?php echo $nom['nom_j']; ?></td>
				<td><?php echo $nom['date_ins']; ?></td>
				<td><?php echo $nom['date_debut']; ?></td>
				<td><?php echo $nom['nb_joueur_min']; ?></td>
				<td><?php echo $nom['nb_joueur_max']; ?></td>
				<td><?php echo $res_nb_inscrit['nb']; ?></td>
				<td><?php echo $nom['lib_type']; ?></td>
		                <td style="text-align:center; ">  <?php /* dans cette case on affiche :
					     soit un bouton "S'incrire" si l'utilisateur connecté n'est pas déjà inscrit
					     soit un message pour dire qu'il est déjà inscrit à cette competition
					    */
					// test : est-il déjà inscrit ?
					$rech_pseudo_inscrit = "SELECT joueur FROM jeux_video.inscrire
								INNER JOIN jeux_video.utilisateur on id_utilisateur=joueur
								WHERE match = ".$nom['match_id']." AND pseudo ='".$_SESSION['pseudo']."';";
					$res_deja_inscrit= $connexion->query($rech_pseudo_inscrit)->fetch();

					if ($res_deja_inscrit['joueur']!=null) {  ?> <span style="color:cornflowerblue;">Inscrit</span>  <?php }
					else { ?>						
					   <form action="confirmation_match.php" method="post">
						<input type='hidden' name='match_id' value="<?php echo $nom['match_id']; ?>"/>				
						<input type="submit" value="S'inscrire" name="inscrire" />
					   </form>
				 <?php } ?></td>
			</tr>

	 <?php    } // fin while  ?>
           
                
            </table><br><br>
            
        
        <form style="margin-bottom:10px;" id="oups" action="competition_create.php" method="POST">
          <span style="margin-left: 500px;" >  <input type="submit" value="Créer une compétition" name="creer_compet" /></span>
              
        </form><br>
	</div>
	<div id="footer_compet">
	<?php include "../include/footer_public.php";  ?>
	</div>
    </div> 
    </body>
</html>
