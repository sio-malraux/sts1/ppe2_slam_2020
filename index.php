<!DOCTYPE html>
<html>
 <?php
    include "include/header.php";
    ?>

  <head>
    <meta charset="utf-8">
    <title>SIO Gaming</title>
    <link rel="stylesheet" href="CSS/home.css">
    <link href="https://fonts.googleapis.com/css?family=Overpass:300,400,600,700,800,900" rel="stylesheet">
  </head>
   <body class="main"> 
     <div >
   
     
      <img class="logo" src="images/main_logo.png"></img>

      <h2 class="titre">Bienvenue sur SIO Gaming</h2>
      <p class="paragraphe">Découvrez SIO Gaming la communauté Wiki multi gaming.</p>
      <p class="paragraphe">¯\_(ツ)_/¯</p>


      <video id="video_background" preload autoplay loop muted="muted" volume="0">
        <source src="video/bg_video.mp4" type="video/mp4">
      </video>
	 
     <br><br><br>
     <p class="credit">* Gameplay - <a class="lien" href="https://www.youtube.com/NoCopyrightGameplays">No Copyright Gameplay</a></p>  
    </div>

        <?php
        include "include/footer.php";
        ?>

  </body>
</html>
